# TDD in Java

## How it works

1. Install [Java and configure JAVA HOME](https://docs.oracle.com/cd/E19509-01/820-3208/inst_cli_jdk_javahome_t/)
1. Install [gradle](https://gradle.org/install/) into your computer
2. Install dependencies using `gradle build`
3. Execute tests using `gradle test`

##  Documentation

### JUnit

Is the tool we will use for testing.

- **Main documentation:** https://junit.org/junit5/
- **Introduction:** https://github.com/junit-team/junit4/wiki/Getting-started
- **Assertions:** https://github.com/junit-team/junit4/wiki/Assertions
- **Stubbing and mocking:** https://semaphoreci.com/community/tutorials/stubbing-and-mocking-with-mockito-2-and-junit
